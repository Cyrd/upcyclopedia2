from app_init import db


class Material(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name_common = db.Column(db.String(80))
    code = db.Column(db.String(10))
    name_official = db.Column(db.String(400))
    symbol = db.Column(db.String(3))
    type = db.Column(db.String(80))
    subtype = db.Column(db.String(80))
    origin = db.Column(db.String(80))
    description = db.Column(db.String(600), nullable=True)
    tech_info = db.Column(db.String(600), nullable=True)
    occurrence = db.Column(db.String(600), nullable=True)
    availability = db.Column(db.String(300), nullable=True)
    producer = db.Column(db.String(100), nullable=True)
    city_of_producer = db.Column(db.String(50), nullable=True)
    website_of_producer = db.Column(db.String(80), nullable=True)
    origin_code = db.Column(db.Integer, nullable=True)

    #
    # def __repr__(self):
    #     return'{}-{}-{}'.format(self.id, self.name_common, self.origin_code)
