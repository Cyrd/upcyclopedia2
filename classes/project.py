from app_init import db

#Comments not indente

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    website = db.Column(db.String(200))
    #code = db.Column(db.String(12))
    name = db.Column(db.String(50))
    designer = db.Column(db.String(500))
    location = db.Column(db.String(80))
    year = db.Column(db.Integer)
    material = db.Column(db.Integer, db.ForeignKey('material.id'), nullable=True)
        #material_type = db.Column(db.String(30))  #
        #material_subtype = db.Column(db.String(50))  #
        #material_origin = db.Column(db.String(50))  #
    additional_materials = db.Column(db.String(800), nullable=True)
    technique = db.Column(db.String(800), nullable=True)
    symbol = db.Column(db.String(3))  #
    #description_eng = db.Column(db.String(2000))
    description_pl = db.Column(db.String(2000))
    #photos = db.Column(db.String(20))
    #movie = db.Column(db.String(50), nullable=True)
