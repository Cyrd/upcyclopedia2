$(function () {
    var i = 0;
    $(".row").hide();
    var keyword = getUrlParameter("id")
    $.get("/query_projects_type/" + keyword, function (data) {
        data.results.forEach(function (tuple) {
            i == 0 ? i = 1 : i = 0;
            if (i == 1) {
                var tile = $(".row").first().clone().appendTo("#tiles").show();
                tile.find("img").first().attr("src", "../static/webpage/images/photos/proj_tiles/p" + tuple.id + ".jpg")
                tile.find("a").first().attr("href", "/project_exact?id=" + tuple.id)
            } else {
                tile = $(".row").last();
                tile.find("img").last().attr("src", "../static/webpage/images/photos/proj_tiles/p" + tuple.id + ".jpg")
                tile.find("a").last().attr("href", "/project_exact?id=" + tuple.id)
            }
        })
        $(".row").first().remove();
    })
    //$(".container-fluid").on("click", ".clickable-row",(function (e) {
    //    document.location = $(this).data("href") + "?id=" + $(this).find("p").eq(3).text();
    //}))
})



function imageError(image) {
    image.onerror = "";
    image.src = "../static/webpage/images/p0.jpg";
    return true;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};