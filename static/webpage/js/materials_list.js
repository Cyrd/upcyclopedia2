$(function () {
    $(".row").hide();
    var keyword = getUrlParameter("id");
    $.get("/query_type/" + keyword, function (data) {
        data.results.forEach(function (tuple) {
            var tile = $(".row").first().clone().appendTo("#tiles").show();
            $(tile.find("p").first().text(tuple.code));
            $(tile.find("p").eq(1).text(tuple.name_common));
            $(tile.find("p").eq(2).text("kategoria: " + tuple.origin));
            $(tile.find("p").eq(3).text(tuple.id)).hide();
            $(tile.find("img").first().attr("src", "../static/webpage/images/photos/mat_tiles/m" + tuple.id + ".jpg"))
        })
        $(".row").first().remove();
    })
    $(".container-fluid").on("click", ".clickable-row",(function (e) {
        document.location = $(this).data("href") + "?id=" + $(this).find("p").eq(3).text();
    }))


})

    $("#to_proj").click (function (e) {
        document.location = "/projects_type" + "?id=" + getUrlParameter("id");
    })

function imageError(image) {
    image.onerror = "";
    image.src = "../static/webpage/images/p0.jpg";
    return true;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};