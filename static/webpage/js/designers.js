$(function () {
    $(".row").first().hide();
    $.get("/query_designers", function (data) {
        data.results.forEach(function (tuple) {
                var tile = $(".clickable-row").first().clone().appendTo("#column").show();
                $(tile.find("p").first().text(tuple["0"]));

            })
        })
        //$(".row").first().remove();
    })
    $(".container-fluid").on("click", ".clickable-row",(function (e) {
        document.location = "/projects_of" + "?id=" + $(this).find("p").first().text();
    }))

    $("#search_button").on("click", (function(event) {
        var text = $("#insert_form").val().toLowerCase();
        $("p").each(function(row) {
            if($(this).text().toLowerCase().indexOf(text) > -1) {
                $(this).parent().show();
            } else {
                $(this).parent().hide();
            }
        })
    }))




function imageError(image) {
    image.onerror = "";
    image.src = "../static/webpage/images/p0.jpg";
    return true;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};