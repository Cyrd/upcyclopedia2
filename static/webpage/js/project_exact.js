$(function () {
    $("#myCarousel").hide();
    var keyword = "p" + getUrlParameter("id");
    $.get("/query_project_by_id/" + keyword, function (data) {
        data.results.forEach(function (tuple) {
            $("#name").text(tuple.name);
            $("#author").text("Autor: " + tuple.designer);
            $("#location").text("Miejsce: " + tuple.location);
            $("#year").text("Rok: " + tuple.year);
            $("#technique").text("Metoda wykonania: " + tuple.technique);
            $("#description").text("Historia projektu: " + tuple.description_pl);
            $("#link").attr("href", tuple.website);

            if(tuple.material != "None") {
                $.get("/query_material_id/m" + tuple.material, function (material) {
                    material.results.forEach (function (line) {
                        $("#material_name").text("Nazwa materialu: " + line.name_official);
                        $("#material_description").text(line.description);
                    })

                })
            } else {
                $("#material").hide();
            }

        })
    })
    $.get("/get_id_images/" + keyword, function (images) {
        if(images.results.length != 0) {
            $("#myCarousel").show();
            var x = 0;
            images.results.forEach(function (url){
                if(x == 0)
                {
                    $(document).find("img").first().attr("src", "../static/webpage/images/photos/" + keyword + "/" + url);
                    x = 1;
                } else
                {
                    var tile = $(".item").first().clone().removeClass("active");
                    tile.appendTo("#carousel_inner");
                    tile.find("img").first().attr("src", "../static/webpage/images/photos/" + keyword + "/" + url);
                }
            });
        }
    });
})


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};