$(function () {
    $("#myCarousel").hide();
    var keyword = "m" + getUrlParameter("id");
    $.get("/query_material_id/" + keyword, function (data) {
        data.results.forEach(function (tuple) {
            $("#code").text(tuple.code);
            $("#name_common").text(tuple.name_common);
            $("#type").text("typ: " + tuple.type);
            $("#subtype").text("rodzaj: " + tuple.subtype);
            $("#origin").text("kategoria: " + tuple.origin);
            $("#description").text(tuple.description);
            $("#tech_info").text(tuple.tech_info);
            $("#availability").text(tuple.availability);
        })
    })
    $.get("/get_id_images/" + keyword, function (images) {
        if(images.results.length != 0) {
            $("#myCarousel").show();
            var x = 0;
            images.results.forEach(function (url){
                if(x == 0)
                {
                    $(document).find("img").first().attr("src", "../static/webpage/images/photos/" + keyword + "/" + url);
                    x = 1;
                } else
                {
                    var tile = $(".item").first().clone().removeClass("active");
                    tile.appendTo("#carousel_inner");
                    tile.find("img").first().attr("src", "../static/webpage/images/photos/" + keyword + "/" + url);
                }
            });
        }
    });
})


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};