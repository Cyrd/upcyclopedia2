# coding=utf-8

import codecs
import csv
import os
from flask import Flask, jsonify, render_template, request
import sys
import json
from classes.project import Project
from app_init import db, app
from classes.material import Material


def origin_to_code(text):
    x = text.lower()
    return {
        'koniec cyklu życia': 0,
        'pozostałości produkcyjne': 1,
        'odpady budowlane i rozbiórkowe': 2,
        'zapasy towarów niechodliwych': 3,
        'pozostałe': 4,
        'materiał odzyskany w wyniku recyklingu': 1,
        'recykling': 1
    }.get(x, 99)


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqldb://root:password@localhost/database'


@app.route('/')
def index():
    return render_template('main.html')


@app.route('/menu_materials')
def imenu_materials():
    return render_template('menu_materials.html')


@app.route('/menu_projects')
def menu_projects():
    return render_template('menu_projects.html')


@app.route('/mcats')
def mcats():
    return render_template('mcats.html')


@app.route('/mtypes')
def types():
    return render_template('mtypes.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/projects')
def project():
    return render_template('projects.html')


@app.route('/projects_type')
def projects_type():
    return render_template('projects_type.html')


@app.route('/projects_of')
def projects_of():
    return render_template('projects_of.html')


@app.route('/contact')
def contact():
    return render_template('contact.html')


@app.route('/materials_list')
def materials_list():
    return render_template('materials_list.html')


@app.route('/category')
def category():
    return render_template('category.html')


@app.route('/query_all_projects')
def query_all_projects():
    tuples = Project.query.all()
    return tuples_to_json(tuples)


@app.route('/query_projects_type/<id>')
def query_projects_type(id):
    tuples = Project.query.filter_by(symbol=id).all()
    return tuples_to_json(tuples)


@app.route('/query_category/<id>')
def query_category(id):
    tuples = Material.query.filter_by(origin_code=id).all()
    return tuples_to_json(tuples)


@app.route('/material_exact')
def materials_exact():
    return render_template('material_exact.html')


@app.route('/designers')
def designers():
    return render_template('designers.html')


@app.route('/project_exact')
def project_exact():
    return render_template('project_exact.html')


@app.route('/material_origin/<mat_or>')
def search_material_origin(mat_or):
    tuples = Material.query.filter_by(origin_code=origin_to_code(mat_or)).all()
    return tuples_to_json(tuples)


@app.route('/query_designers')
def query_designers():
    designers = db.session.query(Project.designer.label("designer")).group_by(Project.designer).all()
    return jsonify(results=designers)


@app.route('/query_material_id/<mat_id>')
def search_material_id(mat_id):
    tuple = Material.query.filter_by(id=int(mat_id[1:])).all()
    return tuples_to_json(tuple)


@app.route('/query_project_by_id/<proj_id>')
def query_project_by_id(proj_id):
    tuple = Project.query.filter_by(id=int(proj_id[1:])).all()
    return tuples_to_json(tuple)


@app.route('/query_projects_of/<name>')
def query_projects_of(name):
    tuple = Project.query.filter_by(designer=name.replace("%", " ")).all()
    return tuples_to_json(tuple)


@app.route('/query_type/<symbol>')
def search_type(symbol):
    tuples = Material.query.filter_by(symbol=symbol).all()
    return tuples_to_json(tuples)


@app.route(
    '/get_id_images/<mat_id>')  ##TUTAJ MIAŁEM NAJWIĘKSZEGO LAGA W HISTORII - TRÓJARGUMETOWY NA PODZIELENIE ID NA LICZBE I ZNAK
def get_id_images(mat_id):
    path = os.path.dirname(os.path.abspath(__file__)) + "/static/webpage/images/photos/" + mat_id
    if os.path.exists(path):
        names = os.listdir(path)
        full_image_paths = [];
        # for name in names:
        # full_image_paths.append(path + "/" + name)
        # full_image_paths.append("../static/webpage/images/photos/m" + mat_id + "/" + name)
        return jsonify(results=names)  # full_image_paths)
    return jsonify(results=[])


def tuples_to_json(tuples):
    if isinstance(tuples, list):
        tab = []
        for t in tuples:
            tab.append(row2dict(t))
        return jsonify(results=tab)
    c = list(tuples)
    return jsonify(results=row2dict(list(tuples)))


def row2dict(row):  # takes a SQLAlchemy tuple and converts it to dictionary
    d = {}
    for column in row.__table__.columns:
        d[column.name] = unicode(getattr(row, column.name))
        c = []
    return d



    # tuples = Material.query.filter_by(origin_code=origin_to_code(material_origin)).all()
    # #return render_template('show_user.html', tuples = tuples)
    # return tuples#jsonify(results = tuples)


# db.drop_all()
# db.create_all()
#
# with open('/var/www/UpCyclopedia2/mat.csv', 'rb') as cat:
#     reader = csv.reader(cat, delimiter=';')
#     for row in reader:
#         m = Material()
#         m.name_common = str(row[0]).decode('windows-1250').encode('utf-8')
#         m.code = str(row[1]).decode('windows-1250').encode('utf-8')
#         m.name_official = str(row[2]).decode('windows-1250').encode('utf-8')
#         m.symbol = str(row[3]).decode('windows-1250').encode('utf-8')
#         m.type = str(row[4]).decode('windows-1250').encode('utf-8')
#         m.subtype = str(row[5]).decode('windows-1250').encode('utf-8')
#         m.origin = str(row[6]).decode('windows-1250').encode('utf-8')
#         m.description = str(row[7]).decode('windows-1250').encode('utf-8')
#         m.tech_info = str(row[8]).decode('windows-1250').encode('utf-8')
#         m.occurrence = str(row[9]).decode('windows-1250').encode('utf-8')
#         m.availability = str(row[10]).decode('windows-1250').encode('utf-8')
#         m.producer = str(row[11]).decode('windows-1250').encode('utf-8')
#         m.city_of_producer = str(row[12]).decode('windows-1250').encode('utf-8')
#         m.website_of_producer = str(row[13]).decode('windows-1250').encode('utf-8')
#         origin_text = str(row[6]).decode('windows-1250').encode('utf-8')
#         m.origin_code = origin_to_code(m.origin)
#         db.session.add(m)
#         db.session.commit()
#
# with open('/var/www/UpCyclopedia2/proj.csv', 'rb') as proj:
#     reader = csv.reader(proj, delimiter=';')
#     for row in reader:
#         p = Project()
#         p.website = str(row[0]).decode('windows-1250').encode('utf-8')
#         p.name = str(row[1]).decode('windows-1250').encode('utf-8')
#         p.designer = str(row[2]).decode('windows-1250').encode('utf-8')
#         p.location = str(row[3]).decode('windows-1250').encode('utf-8')
#         p.year = str(row[4]).decode('windows-1250').encode('utf-8')
#         # !p.origin = str(row[7]).decode('windows-1250').encode('utf-8')
#         temp_material = Material.query.filter_by(subtype=str(row[6]).decode('windows-1250').encode('utf-8')).first()
#         if temp_material is not None:
#             p.material = temp_material.id
#         p.additional_materials = str(row[8]).decode('windows-1250').encode('utf-8')
#         p.technique = str(row[9]).decode('windows-1250').encode('utf-8')
#         p.symbol = str(row[10]).decode('windows-1250').encode('utf-8')
#         p.description_pl = str(row[11]).decode('windows-1250').encode('utf-8')
#         db.session.add(p)
#         db.session.commit()

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
